# README #

Rust learning materials.

### Basics ###

* [Rust book second edition](https://doc.rust-lang.org/nightly/book/)
* [Rust book 第二版中文版](https://kaisery.github.io/trpl-zh-cn/)
* [Rust by examples](http://rustbyexample.com/)
* [24 Days of Rust](http://zsiciarz.github.io/24daysofrust/)
* [Rust Cook Book](https://brson.github.io/rust-cookbook/)

### Advanced ###

* [Achieving warp speed with Rust](https://gist.github.com/jFransham/369a86eff00e5f280ed25121454acec1)
* [Rust Performance Pitfalls](https://llogiq.github.io/2017/06/01/perf-pitfalls.html)
* [FFI：和 C 語言的界面](https://doc.rust-lang.org/1.5.0/book/ffi.html)
* [Implementing cooperative multitasking in Rust](http://gmorenz.gitlab.io/coop.html)
    - [Followup on Reddit](https://www.reddit.com/r/rust/comments/6htknd/blog_implementing_cooperative_multitasking_in_rust/)
* [Bugs You'll Probably Only Have in Rust](https://gankro.github.io/blah/only-in-rust/)

### Resources ###

* [Crates.io](https://crates.io/)
* [Awesome Rust](https://github.com/rust-unofficial/awesome-rust)
* [StackOverflow: 問問題的地方](https://stackoverflow.com/questions/tagged/rust)
* [Rust Reddit: 問問題以及分享最新 Rust 動態的地方](https://www.reddit.com/r/rust/)